import praw
import json

# User-Agent Key
USER_AGENT_KEY = "/r/toonami autoposter for posting the schedule from site (by /u/kevoc2008)"
# Query to remove pre-thread
QUERY_PRE_THREAD = 'Pre-Thread'
# reddit app_id
APP_ID = 'NOT OPEN DUE TO REDDIT TOS'
# App Secret
APP_SECRET = 'NOT OPEN DUE TO REDDIT TOS'
# App Redirect URI
APP_URI = 'http://127.0.0.1:65010/authorize_callback'
# Scopes for the App to take
APP_SCOPES = 'submit read modposts edit'

r = praw.Reddit(USER_AGENT_KEY)
r.set_oauth_app_info(APP_ID, APP_SECRET, APP_URI)
print '-------- URL TO PASTE TO WEB BROWSER AND ACCEPT PERMISSION -------'
print r.get_authorize_url('...', APP_SCOPES, True)
print '------------------------------------------------------------------'
print 'Please place the following to a browser and accept permission, get the access code, and place it in the console and press enter'
approval_key = raw_input("Please Enter Key: ")
access_info = r.get_access_information(approval_key)
print "Debug Log:"
print access_info
print ''
print ''
print "your key is: " + access_info['refresh_token']