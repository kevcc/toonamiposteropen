#Toonami Thread Poster

----

The Toonami thread poster is a python script that pulls the schedule JSON data from www.adultswim.com/schedule/, parses through the JSON data for the hours between 12:00AM to 3:30AM Eastern Tims and pushes the message to reddit in the markdown language. 

**Implementation**

Before implementing the system onto a service, you must get an API token from Reddit. See https://github.com/reddit/reddit/wiki/OAuth2 for more information

The implementation was designed for Heroku, though the python script can be implemented on a local system, raspberry pi or a VPS. The two main python scripts you want to be concerned of is runOnce.py and runThread.py. runOnce.py is a script that is run once after generating an API Token from Reddit, and will return you the refresh token to continue getting new api oauth tokens past 1 hour. runThread is the script that will take the API tokens, pull the JSON data from AS, and post the thread onto the subreddit. 

To schedule the script to run in heroku at a certain time, see https://devcenter.heroku.com/articles/scheduler

To schedule the script to run in a linux environment, see unix "man cron" 

The server.py file is only used for Heroku to create a webapp that complies with Heroku services. It is not to be used for other services. 

**Bugs**

If you notice a bug, please use Bitbucket's Issue tracker to track issues and correct issues. 

**Not tested**:

* Daylight Savings Time (it should work since it is converting the UTC Timestamp to local Eastern Time using standard libraries).