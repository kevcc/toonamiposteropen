from HTMLParser import HTMLParser
from datetime import datetime,timedelta
from pprint import pprint
import time
import urllib2
import json
import praw
import pytz
import os



# static values
# Number of hours to delay
HOURDELAY = 3
# Toonanmi Start Time (in military time)
STARTTIME = 2330
# Toonami End Time (in military time)
ENDTIME = 0330
# The subreddit to post
SUBREDDIT = '/r/toonami'
# Heading Information
HEADING_INFORMATION = '####WELCOME TOONAMI FAITHFUL!\n\n**What\'s on tonight**\n\n---\n\n| Time | Show | Episode | Number |\n|----------|----------|----------|----------|\n'
# Footer Information
FOOTER_INFORMATION = '\n---\n\nAnd don\'t forget to hashtag (#) your favorite show on Twitter while it\'s on air! #Toonami\n\n---\n\nRemember the rules, have fun, and stay gold.\n\n**Only Toonami.**\n\n---'
# Fail Message
FAIL_MESSAGE_GET = "Failed to get the JSON Data From Adult Swim. \nEither the site is down, or our server scraper is failing. \nNo need for alarm, but just notify the mods about the issue."
# Disclaimer
DISCLAIMER = '\n\n'
# User-Agent Key
USER_AGENT_KEY = "UNIQUE USER AGENT KEY TO PREVENT API THRESHOLDS"
# Query to remove pre-thread
QUERY_PRE_THREAD = 'Pre-Thread'
# Query to get the first characters of Pre-thread
PRE_THREAD_TITLE = "The Official"
# reddit app_id
APP_ID = 'NOT OPEN DUE TO REDDIT TOS'
# App Secret
APP_SECRET = 'NOT OPEN DUE TO REDDIT TOS'
# App Redirect URI
APP_URI = 'http://127.0.0.1:65010/authorize_callback'
# Scopes for the App to take
APP_SCOPES = 'submit modposts edit read'
# Refresh token to get new app runtime
APP_REFRESH_CODE = 'NOT OPEN DUE TO REDDIT TOS'
# Offset to test changes
OFFSET = 100
# URL to scrape information
URL_STRING = 'http://www.adultswim.com/videos/schedule'
# JSON Directory for the episode count dictionary
SHOW_DIR = './showDict.json'

def PostToReddit(schedule):
	redditdatetemp = datetime.now()
	redditdatetemp = redditdatetemp - timedelta(hours=6)
	redditdate =  redditdatetemp.strftime("%B %d, %Y")
	reddittitle = "[Official Toonami Discussion Thread for " + str(redditdate) + "]"
	redditdescription = HEADING_INFORMATION + schedule + FOOTER_INFORMATION + DISCLAIMER
	r = praw.Reddit(USER_AGENT_KEY, disable_update_check=True)
	r.set_oauth_app_info(APP_ID, APP_SECRET, APP_URI)
	r.refresh_access_information(APP_REFRESH_CODE)
	print reddittitle
	print redditdescription
	post = r.submit (SUBREDDIT, reddittitle, text=redditdescription)
	searchresults = r.search(QUERY_PRE_THREAD, subreddit=SUBREDDIT[3:],
                                          sort='new', limit=100,
                                          syntax='cloudsearch')
	for post in searchresults:
		if str(post.title).startswith(PRE_THREAD_TITLE):
			post.remove()


def getCount(showinput, episodenameinput):
	with open(SHOW_DIR) as data_file:
		data = json.load(data_file)	
		try:
			for show in data['show']:
				if show['name'] == showinput:
					for episode in show['episode']:
						if episodenameinput.startswith(episode['title'][:((len(str(episode['title'])) / 4) * 2)]):
							episodeformat = str(episode['number'])
							if show['totalepisode'] >= 2000:
								return episodeformat + " out of ongoing series"
							else:
								return episodeformat + " out of " + str(show['totalepisode'])
					return "Unknown episode count"
			return "Unknown"
		except:
			return "Get Episode Number Failed"
	return "Unknown"

# create a subclass and override the handler methods
class MyHTMLParser(HTMLParser):
	
	def handle_starttag(self, tag, attrs):
		pass

	def handle_endtag(self, tag):
		pass

	def handle_data(self, data):
		stringDescription = ""
		run_script = False
		s = data.strip()
		if s.partition(' ')[0] == 'AS_INITIAL_DATA':
			t = s.partition(' ')[2]
			if t[:2] == '= ':
				type = json.loads(t[2:-1])
				for keyday in type["onair_schedule"].keys():
					if datetime.strptime(str(keyday), '%y-%m-%d').weekday() == 5:
						for schedule in type["onair_schedule"][keyday]:
							time = datetime.fromtimestamp(schedule["timestamp"])
							utc_dt = datetime.utcfromtimestamp(schedule["timestamp"]).replace(tzinfo=pytz.utc)
							tz = pytz.timezone('America/New_York')
							time = utc_dt.astimezone(tz)
							timevalue = int(time.strftime("%H%M"))
							if (STARTTIME != 0000):
								if (timevalue >= STARTTIME):
									#print "**" + time.strftime("%I:%M %p") + "** | **" + schedule["title"] + "** | *" + schedule["episode_name"] + "* "
									stringDescription +="**" + time.strftime("%I:%M%p") + "** | **" + schedule["title"].strip() + "** | *" + schedule["episode_name"].strip() + "* | " + getCount(schedule["title"].strip(), schedule["episode_name"].strip()) + " |\n"
									run_script = True
							if (timevalue < ENDTIME + OFFSET):
								#print "**" + time.strftime("%I:%M %p") + "** | **" + schedule["title"] + "** | *" + schedule["episode_name"] + "* "
								stringDescription +="**" + time.strftime("%I:%M%p") + "** | **" + schedule["title"].strip() + "** | *" + schedule["episode_name"].strip() + "* | " + getCount(schedule["title"].strip(), schedule["episode_name"].strip()) + " |\n"
								run_script = True
		if run_script == True:
			PostToReddit(stringDescription)
		run_script = False

try: 
	response = urllib2.urlopen(URL_STRING)
except urllib2.HTTPError as e:
	print FAIL_MESSAGE_GET
except urllib2.URLError as e:
	print FAIL_MESSAGE_GET
else:	
	if response.getcode() == 200:
			data = response.read()
			parser = MyHTMLParser()
			parser.feed(data)
response.close()
