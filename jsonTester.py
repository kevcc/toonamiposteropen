#!/usr/bin/python
import json
import sys
from pprint import pprint

if len(sys.argv) < 2:
	print 'Arguments'
	print 'jsonCreator.py jsonfile ...'
else:
	pos = 0
	d = {}
	for arg in sys.argv:
		if pos != 0:
			with open(arg) as data_file:
				data = json.load(data_file)	
				for show in data['show']:
					if show['name'] == "One-Punch Man":
						print "found show"
						for episode in show['episode']:
							if episode['title'] == "Unyielding Justice":
								print episode['number']

		pos = pos + 1